[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您在华为云弹性云服务器 ECS 中快速搭建跨境卫士VPS版云上店铺管理环境，跨境卫士VPS版是一款针对外贸、跨境电商的VPS云服务器管理软件，帮助解决跨境企业使用VPS远程桌面管理店铺中遇到的卡顿、掉线、账号安全、批量管理、团队协作等问题，让跨境店铺运营更加流畅。通过沙盒技术和固定路由网络，长效、安全、专业的管理亚马逊、eBay、Wish、速卖通等的电商平台的账号。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/deploying-kuajingvs-to-manage-stores.html

**架构图**
---------------
![方案架构](./document/deploying-kuajingvs-to-manage-stores.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建1台弹性云服务器 ECS，安装跨境卫士VPS版，使店铺管理工作更便捷。
2. 创建弹性公网IP，用于提供访问公网和被公网访问能力。
3. 创建安全组，通过设置安全组规则，仅允许白名单用户接入弹性云服务器，保护店铺环境安全。
此外，您可以通过购买使用云监控服务来监测弹性云服务器运行状态，也可以通过购买云备份服务，对弹性云服务器进行数据备份。

**组织结构**
---------------

``` lua
huaweicloud-solution-deploying-kuajingvs-to-manage-stores
├── deploying-kuajingvs-to-manage-stores.tf.json -- 资源编排模板
```

**开始使用**
---------------
1、通过本地RDP远程连接到弹性云服务器（host填写弹性云服务器绑定的弹性公网IP），登录弹性云服务器并开始管理自己的跨境电商店铺。

图1 远程连接

![SSL证书](./document/readme-image-001.png)

2、进入云服务器，可以看到跨境卫士VPS版浏览器。

图2 虚拟IP及其绑定的公网IP

![虚拟IP及弹性公网EIP](./document/readme-image-002.png)

3、登录或者注册账号，参照[部署指南](https://support.huaweicloud.com/kuajingvs-internet/kuajingvs_01.html)使用。
